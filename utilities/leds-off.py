from sense_hat import SenseHat
from time import sleep

sense = SenseHat()

# This script turns on all LEDs on at half brightness for one second, then turns them all off.
# Can be used to turn off LEDs left on after a program finishes or to test the LEDs.

sense.low_light = True
sense.clear(255, 255, 255)
sleep(1)
sense.clear()
