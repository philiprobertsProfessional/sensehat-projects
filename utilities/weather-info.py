from sense_hat import SenseHat

sense = SenseHat()

print("Humidity: " + str(sense.humidity) +"\nPressure: " + str(sense.pressure) + "\nTemperature(Humidity): " + str(sense.get_temperature_from_humidity()) + "\nTemperature(Pressure): " + str(sense.get_temperature_from_pressure()))
