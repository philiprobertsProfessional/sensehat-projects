const http = require('http');
const mysql = require('mysql');
const hostname = '127.0.0.1';
const port = 3000;

//Create connection object for MariaDB 
var con = mysql.createConnection({
  host: "localhost",
  user: "pi",
  password: ""
});

//Connect to MariaDB
con.connect(function(err) {
	if (err) throw err;
	console.log("Connected to mysql");
});

//Select weather database
con.query("USE weather;", function (err, result) {
	if (err) throw err;
	console.log("Using DB weather");
});

//Web Server
http.createServer((request, response) => {
	
	//Create columns of html table string
	var table = "<table><tr><th>id</th><th>date</th><th>Humidity</th><th>Temperature(Humidity)</th><th>Pressure</th><th>Temperature(Pressure)</th></tr>";
	
	//Query data from database
	con.query("SELECT * FROM weatherinfo;", function (err, result) {
		if (err) throw err;
		console.log("SQL Query Success");
		//Loop through every row in table and add it to html table string
		for(var propertyName in result)	{
			table += "<tr><th>" + result[propertyName].id + "</th><th>" + 
				(result[propertyName].weatherdate) + "</th><th>" + 
				result[propertyName].humidity + "</th><th>" + 
				result[propertyName].humiditytemp + "</th><th>" + 
				result[propertyName].pressure + "</th><th>" + 
				result[propertyName].pressuretemp + "</th><tr>";
		}
		table += "</table>";
	
	//Send table to client and end response
	response.write(table);
	response.end();
	
	});

}).listen(3000);
