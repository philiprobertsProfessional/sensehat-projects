import mysql.connector as mariadb
from sense_hat import SenseHat
import sys

# Create sense object to interface with senseHAT
sense = SenseHat()

# Connect to MariaDB database
try:
    conn = mariadb.connect(
        user="pi",
        password="",
        host="localhost",
        database="weather"

    )
except mariadb.Error as e:
    print("Error connecting to MariaDB Platform: " + str(e))
    sys.exit(1)

#Send data to database
conn.autocommit = True

cur = conn.cursor()

cur.execute("USE weather;")
insertcmd = ("INSERT INTO weatherinfo(weatherdate, humidity, humiditytemp, pressure, pressuretemp)" + " VALUES (" + " NOW(), " + str(sense.get_humidity()) + ", " + str(sense.get_temperature_from_humidity()) + ", " + str(sense.get_pressure()) + ", " + str(sense.get_temperature_from_pressure()) + ");")
print("MYSQL QUERY: " + insertcmd)
cur.execute(insertcmd)
conn.close()

#print(cur.__dict__)
